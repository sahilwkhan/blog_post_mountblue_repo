---
title: Pass by value and Pass by reference in JavaScript
date: 2023-03-02
tags:
---

In this blog, we will discuss how pass-by-value and pass reference work in JavaScript language. Understanding the concept of how the values are passed inside a function makes it easier to avoid errors regarding unexpected value modification.

<br/>

##  Pass by value in JavaScript

While writing code in any language, we often use functions to make our code modular and reduce lines for performing repetitive tasks. Functions are a group of statements that take the defined number of parameters, varying number of parameters, or no parameters for its functioning. 

When using functions to provide output using given data, first we pass the input value to the function as an argument, and then the function displays or returns an output. So while calling a function in the case of Pass by Value, a copy of variables is created inside the function. Any operations performed on these variables, will not change the original variable used for calling functions. 

Consider the example shown below, where a code is written in Javascript.

``` 
function passByValue(fruit,firstNumber,secondNumber){
    firstNumber = 10;
    secondNumber = 20;
    fruit = "WaterMelon";
    console.log("Value of variables when passed to passByValue function : ");
    console.log( `Fruit = ${fruit}, First Number = ${firstNumber}, Second Number = ${secondNumber}\n`);
}

let fruit = "Apple";
let firstNumber = 5;
let secondNumber = 10;
console.log("Value of variables before calling passByValue function : ");
console.log( `Fruit = ${fruit}, First Number = ${firstNumber}, Second Number = ${secondNumber}\n`);

passByValue(fruit, firstNumber, secondNumber);

console.log("Value of variables after passByValue is called :");
console.log( `Fruit = ${fruit}, First Number = ${firstNumber}, Second Number = ${secondNumber}`);

```
Output :

```
Value of variables before passing to passByValue function : 
Fruit = Apple, First Number = 5, Second Number = 10

Value of variables when passed to passByValue function : 
Fruit = WaterMelon, First Number = 10, Second Number = 20

Value of variables after passByValue is called :
Fruit = Apple, First Number = 5, Second Number = 10

```
As represented in the above code snippet, all variables that were passed to the pass-by-value function as arguments were changed inside the function. After the function execution is completed, variables have the same values they had before calling the function. From this example, we can understand the copy of variables inside the passByValue function that were modified inside the function cannot change the original value of the variable.

It should be remembered that in the javascript pass-by-value only works, for primitive data types like **numbers**, **strings**, **boolean**, and **undefined**.

<br/>

##  Pass by Reference in JavaScript

Unlike pass-by-value, in the case of 'Pass by Reference', the original object that is given as arguments to a function can be changed from inside the function. This happens because rather than a copy of a variable, a reference or an address is given as an argument to a function. So any changes made through function are directly made to the original object since it was passed by its address. 

To avoid this variable manipulation through function, we need to explicitly create and input a copy of variable data and then pass it to a function.

This phenomenon can be understood by the javascript code below,

```
function passByReference(listOfFruits,checkObject){
    let index = listOfFruits.indexOf("Banana");
    if(index !== -1){
        listOfFruits[index] = "Peach";
    }
    checkObject['Object State'] = "Modified";
    console.log("2. Value of list of fruits and object inside passByReference function : ");
    console.log( `List of Fruits = [ ${listOfFruits} ]`);
    console.log( checkObject,'\n');
}

let listOfFruits = ["Apple", "Banana","Pineapple"];
let listOfFruitsCopy = listOfFruits;
let checkObject = {"Object State" : "Unchanged"};
let checkObjectCopy = checkObject;

console.log("1. Value of list of fruits and object before calling passByReference function : ");
console.log( `List of Fruits = [ ${listOfFruits} ]`);
console.log( checkObject,'\n');
    
passByReference(listOfFruitsCopy,checkObjectCopy);
 
console.log("3. Value of list of fruits and object after calling passByReference function : ");
console.log( `List of Fruits = [ ${listOfFruits} ]`);
console.log( checkObject,'\n');



```

Output :

```
1. Value of list of fruits and object before calling passByReference function : 
List of Fruits = [ Apple,Banana,Pineapple ]
{ 'Object State': 'Unchanged' } 

2. Value of list of fruits and object inside passByReference function : 
List of Fruits = [ Apple,Peach,Pineapple ]
{ 'Object State': 'Modified' } 

3. Value of list of fruits and object after calling passByReference function : 
List of Fruits = [ Apple,Peach,Pineapple ]
{ 'Object State': 'Modified' } 

```

From the output above, we can observe that the value of an array and object was modified inside a function, even when we assigned the original variables of the array and object to other variables. 

The reason for this modification is that, when we assigned the 'listOfFruits' and 'checkObject' object and array to their respective copy variables 'listOfFruitsCopy' and  'checkObjectCopy', they did not store their values inside a new address like in pass-by-value, but assigned same address to their copy variables. When we passed the copy variables as arguments to the 'passByReference' function, it repeated the same step of passing the address to parameters rather than creating and storing their value in new variables. Both parameters represent the address of the original array and object, so any modification is directly taking place at the original object and array. 

In JavaScript, Non-primitive or Reference data types such as functions, objects, and arrays are passed according to Pass by Reference technique. To pass the object and arrays without getting them modified inside a function, we have to pass their cloned copy as an argument to a function.

Let's consider the previous example for cloning objects and arrays,

```
function passByReference(listOfFruits,checkObject){
    let index = listOfFruits.indexOf("Banana");
    if(index !== -1){
        listOfFruits[index] = "Peach";
    }
    checkObject['Object State'] = "Modified";
    console.log("2. Value of list of fruits and object inside passByReference function : ");
    console.log( `List of Fruits = [ ${listOfFruits} ]`);
    console.log( checkObject,'\n');
}

let listOfFruits = ["Apple", "Banana","Pineapple"];
let listOfFruitsCopy = Object.assign([], listOfFruits);
let checkObject = {"Object State" : "Unchanged"};
let checkObjectCopy =  Object.assign({}, checkObject);

console.log("1. Value of list of fruits and object before calling passByReference function : ");
console.log( `List of Fruits = [ ${listOfFruits} ]`);
console.log( checkObject,'\n');
    
passByReference(listOfFruitsCopy,checkObjectCopy);
 
console.log("3. Value of list of fruits and object after calling passByReference function : ");
console.log( `List of Fruits = [ ${listOfFruits} ]`);
console.log( checkObject,'\n');

```
Output :

```
1. Value of list of fruits and object before calling passByReference function : 
List of Fruits = [ Apple,Banana,Pineapple ]
{ 'Object State': 'Unchanged' } 

2. Value of list of fruits and object inside passByReference function : 
List of Fruits = [ Apple,Peach,Pineapple ]
{ 'Object State': 'Modified' } 

3. Value of list of fruits and object after calling passByReference function : 
List of Fruits = [ Apple,Banana,Pineapple ]
{ 'Object State': 'Unchanged' } 

```

We can see from the output, that the original object and array did not get modified, because we used the 'Object.assign()' method to create a copy of 'listOfFruits' array and 'checkObject' object and assigned them values to 'listOfFruitsCopy' and 'checkObjectCopy' variables. Their addresses are different from each other, hence no change is conveyed to the original object and array.

## Summary :-

1. In JavaScript, pass-by-value creates a copy of data inside the functions, therefore the changes inside the functions do not affect the original values.
2. In the case of pass-by-reference, the address of data is passed inside functions, hence change to data inside functions alters the original values.
3. Javascript uses pass-by-value for primitive data types and pass-by-reference for non-primitive data types.




