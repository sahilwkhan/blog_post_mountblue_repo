---
title: Sort Method in JavaScript 
date: 2023-03-07 
tags: 
---

## What is the Sort method?

A sort method in JavaScript is a built-in array method that is used to sort the elements of an array and then it returns an address of the original array which is sorted. Sort array does not create a copy of the array given as input, but it sorts the original array.

An example of the array sort method is shown in the code below:-
```
const fruits = ["Apple", "Mango", "Peach", "Orange", "Banana"]
fruits.sort();
console.log(fruits);
```
Output :
```
[ 'Apple', 'Banana', 'Mango', 'Orange', 'Peach' ]
```
As we can see from the output, the sort method sorted the fruits array in ascending alphabetical order. 

The sort method has a default sorting order as ascending which is done by converting the elements to string and then comparing the UTF-16 code unit values of elements to sort them.

Below is the syntax of the default sort method;

```
// Default sort method without any parameter
Array.sort();
```

The default condition of comparing the UTF-16 code unit values of array elements occurs when no function containing sorting logic is given to the sort method as a parameter. 

However, the sort default conditions are not preferred in many cases, especially when dealing with numbers and different case strings as array elements.

Consider the previous fruits code example to sort fruit names alphabetically,
```
const fruits = ["apple", "Mango", "Peach", "Orange", "Banana"]
fruits.sort();
console.log(fruits);
```
Output :
```
[ 'Banana', 'Mango', 'Orange', 'Peach', 'apple' ]
```
In the output, the 'apple' string is in lowercase as compared to other elements whose first letters in capitalized. Since the UTF-16 Value of all capital letters is less than the lowercase letters, 'apple' is sorted as the last element. A similar, problem occurs with the number array as shown in the code below,

```
numberArray1 = ['1','5','2','4','3'];
numberArray2 = ['1','30','2','70','4','8'];
numberArray1.sort();
numberArray2.sort();
console.log("Array 1 :", numberArray1);
console.log("Array 2 :", numberArray2);
```
Output :

```
Array 1 : [ '1', '2', '3', '4', '5' ]
Array 2 : [ '1', '2', '30', '4', '70', '8' ]
```
In the output, the first number string array is sorted as per the expectation in the ascending format, although in the second array, the sort method failed to order the elements in an ascending manner. 

The sort converts elements to strings and compares the UTF values of elements in the default condition. In UTF comparison, the value of '30' comes before '4' because only the first letter or digit is being considered. This fails to sort the array values and therefore the default sort method should is not preferred.

To overcome this disadvantage of the sort method, an input function should be given to the sort method as a parameter, that contains the logic for sorting the elements. 

The upcoming modules will discuss the methods and examples of how to use the sort method by using callback functions on different datatype array elements.
<br>

## Sort method with callback functions

Let us understand, the syntax of the sort method with functions as parameters,

```
// Sort method with a function as a parameter
function sortingLogic(element1, element2){
    // Condition for sorting elements
};
Array.sort(sortingLogic);

// Sort method with Inline function
Array.sort(function sortingLogic(element1, element2){
    // Condition for sorting elements
});

// Sort method having arrow function 
Array.sort( (element1, element2) => {// Condition for sorting elements});
```
In a sort method, a callback function is an optional parameter but becomes crucial when we need a correct sorted array, and avoid the unexpected results as mentioned previously. 

This callback function provides the logic, according to which our array elements will be sorted. The callback function has two parameters that represent the two elements of an array. Using these parameters, we can code our logic for sorting.

There are some points to be remembered while coding the sort logic inside the callback functions. When we sort an array of elements, we have mainly three situations that are,
1. First element need to be sorted before the second element,
2. Second element need to be sorted before the first element,
3. No sorting is required for current elements.

These situations are dealt with by using the return property of the functions, where
1. Negative value means to sort the first element before the second element
2. Positive value means to sort the second element before the first element
3. If returned value === 0, then sorting of the selected two elements is not needed.
<br>

## Sorting the array of strings 

In a situation, where we need to sort the array of strings according to different ways like in descending alphabetical order, length of string, etc we need to input a function containing the logic to sort as an argument to sort method.

Assume we have an array of employee names to be sorted as shown below;

```
const employeeNames = ["John", "Rick", "Anna", "Steve"];
```
If we need to sort this array in an ascending alphabetical format, it can be done by using the sort method without passing functions.
```
const employeeNames = ["John", "Rick", "Anna", "Steve"];
employeeNames.sort();
console.log(employeeNames);
```
Output :
```
[ 'Anna', 'John', 'Rick', 'Steve' ]
```

Let's think about a situation, where we need the array in descending order. The solution for this problem will use a callback as shown below,

```
const employeeNames = ["John", "Rick", "Anna", "Steve"];
// comparison function to sort in descending alphabetical order
function descending_sort(firstEmployee, secondEmployee){
    if (firstEmployee > secondEmployee ){
        return -1;
    }
    else if(firstEmployee < secondEmployee){
        return 1;
    }
    else{
        return 0;
    }
}
let  employeeData = employeeNames.sort(descending_sort);
console.log(employeeData);
```

Output : 
```
[ 'Steve', 'Rick', 'John', 'Anna' ]
```
<br>

## Sorting array of Different Case Strings

In the default sort method, the method considers the first letter case of a string to do sorting, or in other words, it performs case-sensitive sorting. With the passing of the callback function to the sort method, we can enable the sort method to perform a case Insensitive sort, where the string can be sorted irrespective of its case.

Below is an example of using the sort method with different case strings,

```
let fruits = ["Peach", "waterMelon", "orange", "Mango", "Banana", "apple"];

// Using Default sort method 
let defaultFruitSorting = [...fruits].sort();
console.log("Sorted array using default sort :")
console.log(defaultFruitSorting,"\n");

// Using callback function as parameter
let firstFruit, secondFruit;
let callbackFruitSorting = [...fruits].sort((firstFruit, secondFruit) =>{
    if ( firstFruit.toLowerCase() < secondFruit.toLowerCase() ){
        return -1;
    }
    else if(firstFruit.toLowerCase() >  secondFruit.toLowerCase()){
        return 1;
    }
    else{
        return 0;
    }
});
console.log("Sorted array using callback function sort :")
console.log(callbackFruitSorting);
```
Output :
```
Sorted array using default sort :
[ 'Banana', 'Mango', 'Peach', 'apple', 'orange', 'waterMelon' ] 

Sorted array using callback function sort :
[ 'apple', 'Banana', 'Mango', 'orange', 'Peach', 'waterMelon' ]
```
In this code, spread syntax was implemented to create a shallow copy, so that we can avoid making changes to the original array.

From the output, we can see that the default sort method as mentioned was not able to sort different case strings, whereas after passing callback functions, we converted the two fruit strings being compared into lowercase, so we got the expected sorted array.
<br>

## Sorting array of Integers

We already have seen an example where the default sort method, sorted a number string of '30' before '4' for an ascending array because it only considered the first digit of the number string. To sort the integer string or integer array properly, callbacks are required that will define the sorting logic.

``` 
let integers = [1,2,3,40,4,500,5,625,7];

// Using Default sort method 
let defaultIntegerSorting = [...integers].sort();
console.log("Sorted array using default sort :")
console.log(defaultIntegerSorting,"\n");

// Using callback function as a parameter
let firstInteger, secondInteger;
let callbackIntegerSorting = [...integers].sort((firstInteger, secondInteger) =>{
    if ( firstInteger < secondInteger ){
        return -1;
    }
    else if(firstInteger > secondInteger){
        return 1;
    }
    else{
        return 0;
    }
});
console.log("Sorted array using callback function sort :")
console.log(callbackIntegerSorting);
```
Output :
```
Sorted array using default sort :
[
  1,   2,   3, 4, 40,
  5, 500, 625, 7
] 

Sorted array using callback function sort :
[
  1,  2,   3,   4, 5,
  7, 40, 500, 625
]
```
From the output, we can see that the default sort method sorted only by considering only the first digit of integers, and on the other side, the callback function sorted the integers according to their values, which was the output expected.

## Sorting array of Floating point numbers

Just like we sorted the integers array successfully, we can also easily sort the array of Floating point numbers using the callback functions. 
```
let floatingPointNumbers = [1.5, 4.8,55.8, 5.5 ,2.5,6.7,14.5,114.6];

// Using Default sort method 
let defaultFloatingNumberSorting = [...floatingPointNumbers].sort();
console.log("Sorted array using default sort :")
console.log(defaultFloatingNumberSorting,"\n");

// Using callback function as a parameter
let firstFloatingNumber, secondFloatingNumber;
let callbackFloatNumberSorting = [...floatingPointNumbers].sort((firstFloatingNumber, secondFloatingNumber) =>{
    if ( firstFloatingNumber < secondFloatingNumber ){
        return -1;
    }
    else if(firstFloatingNumber > secondFloatingNumber){
        return 1;
    }
    else{
        return 0;
    }
});
console.log("Sorted array using callback function sort :")
console.log(callbackFloatNumberSorting);
```
Output: 
```
Sorted array using default sort :
[
   1.5, 114.6, 14.5,
   2.5,   4.8,  5.5,
  55.8,   6.7
] 

Sorted array using callback function sort :
[
   1.5,   2.5,  4.8,
   5.5,   6.7, 14.5,
  55.8, 114.6
]
```

The default sort method again outputs the same error of sorting the floating point numbers just by considering the first digit of the number. In the case of passing the callback function, we compared the values of floating point numbers, and hence we got the correct sorted array.

## Sort array of Object 

Until now we have been sorting the array of primitive data type elements, but the implementation of the sort method can also be done to sort an array of objects also. To sort the objects inside an array, we need to first use either Dot or Bracket notation to access the keys, whose values will be used to sort the objects inside the array. 

Let us consider an array, containing the objects having a first name and last name keys. We will sort these objects alphabetically based on the first name key of every object and salary as well.

```
let employeeArray = [{name : "John", lastName : "lewis", salary : 6000}, {name : "Anna", lastName : "Pinsent", salary : 4000}, {name : "Roman", lastName : "Sedcole", salary : 10000},{name : "Valera", lastName : "Hinemoor", salary : 8000 }];



// Sorting object based on name
let firstEmployee, secondEmployeee;
let sortEmployeeObjectsOnName = [...employeeArray].sort((firstEmployee, secondEmployeee) =>{
    return firstEmployee.name.localeCompare(secondEmployeee.name);
});
console.log("Sorted employee objects based on name :")
console.log(sortEmployeeObjectsOnName,"\n");
// Sorting object based on salary

let sortEmployeeObjectsOnSalary = [...employeeArray].sort((firstEmployee, secondEmployeee) =>{
    return firstEmployee.salary  - secondEmployeee.salary
});
console.log("Sorted employee objects based on salary :")
console.log(sortEmployeeObjectsOnSalary);
```
Output :
```
Sorted employee objects based on name :
[
  { name: 'Anna', lastName: 'Pinsent', salary: 4000 },
  { name: 'John', lastName: 'lewis', salary: 6000 },
  { name: 'Roman', lastName: 'Sedcole', salary: 10000 },
  { name: 'Valera', lastName: 'Hinemoor', salary: 8000 }
] 

Sorted employee objects based on salary :
[
  { name: 'Anna', lastName: 'Pinsent', salary: 4000 },
  { name: 'John', lastName: 'lewis', salary: 6000 },
  { name: 'Valera', lastName: 'Hinemoor', salary: 8000 },
  { name: 'Roman', lastName: 'Sedcole', salary: 10000 }
]
```

Similar to the previous example, the object in the 'employeeArray' were sorted according to their name in ascending order, but this time instead of writing an if-else statement to return the negative, positive, and zero values, the localeCompare() method was used. This is a string method that checks whether the string on which the method is called, comes before, after, or is the same as the string given as the argument. It returns the same values of 1, -1, and zero for which we had written an if-else statement in previous examples. 

After sorting the objects based on names, the shallow copy was created again using spread syntax, to sort it based on the salary of employees. In this sorting, the difference between the salaries of two employees was checked. Since we needed to sort salaries in ascending order, the salary of 'secondEmployee' was subtracted from 'firstEmployee', and these differences provide the same three values of positive, negative, and zero if salaries are the same. Based on the result of this operation, lower salary is sorted before higher salary as shown in the output.


## Sort array of Object using multiple keys

Now that we have seen how to sort the object based on a single key, in this section we will sort the objects using multiple keys. 

Given below is the example of an array that has the object of IPL teams and the number of times they won the IPL trophy.
```
let IPLteams = [
    {team : "Rajasthan Royals", trophies : 1},
    {team : "Chennai Super Kings",trophies : 4},
    {team : "Deccan Chargers",trophies : 1},
    {team : "Kolkata Knight Riders",trophies : 2},
    {team : "Mumbai Indians",trophies : 4},
    {team : "Sunrisers Hyderabad",trophies : 1},
];
```
In this data, we can see that many teams have the same number of trophies. So, along with sorting the teams in descending order to trophies, we want to sort the teams by their names in alphabetical order if the trophy count is the same. In this case, we have the option of using the sort twice, but it will make the code lengthy and redundant. 

Here, we can combine sorting criteria. This is shown in the code below;

```
let IPLteams = [
    {team : "Rajasthan Royals", trophies : 1},
    {team : "Chennai Super Kings",trophies : 4},
    {team : "Deccan Chargers",trophies : 1},
    {team : "Kolkata Knight Riders",trophies : 2},
    {team : "Mumbai Indians",trophies : 4},
    {team : "Sunrisers Hyderabad",trophies : 1},
];

// Sorting object based on trophies and name
let firstTeam, secondTeam;
let sortEmployeeObjectsOnName = [...IPLteams].sort((firstTeam, secondTeam) =>{
    return secondTeam.trophies - firstTeam.trophies || firstTeam.team.localeCompare(secondTeam.team);
});
console.log("Teams Sorted based on trophies and name :")
console.log(sortEmployeeObjectsOnName,"\n");
```
Output :
```
Teams Sorted based on trophies and name :
[
  { team: 'Chennai Super Kings', trophies: 4 },
  { team: 'Mumbai Indians', trophies: 4 },
  { team: 'Kolkata Knight Riders', trophies: 2 },
  { team: 'Deccan Chargers', trophies: 1 },
  { team: 'Rajasthan Royals', trophies: 1 },
  { team: 'Sunrisers Hyderabad', trophies: 1 }
] 
```
From the output, we can see that the teams are first sorted according to the number of trophies they won, and if their trophies count is the same, then they are sorted in ascending alphabetical order. 
