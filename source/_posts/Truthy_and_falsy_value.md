---
title: Truthy and Falsy values
date: 2023-02-22
tags:
---

## Truthy Values

In Javascript language, Truthy value is a term that is used for the boolean value 'true'.  In programming languages, any non 'false' or falsy value is taken as a truthy value.

Javascript analyzes the code sections where boolean values occur in the form of comparison operators, logical operators, numerical values, or directly through 'true' and 'false' keywords, etc, and interprets its values as either a truthy or a falsy value.

In Javascript, objects, strings, keywords, functions, and the number itself can be used to get true or false boolean values without being expressed explicitly. Javascript processes the non-explicit 'true' outputs by using 'Type coercion' which means the value is converted to another data type without being need to define the conversion.

The code snippet given below will help you understand how the data types in javascript are utilized, to avoid using 'true' and 'false' keywords frequently.

``` 
truthy_value = [{},[],{name: 1},[1],['1'],100, "0", "false", Infinity,-Infinity, true, new String(), -100, 1n, 1.1, -1.1, "true", " "]
 
 for ( let index = 0; index < truthy_value.length; index++){
    if (truthy_value[index]){
    console.log(truthy_value[index] , " == true");
    }
    else{
    console.log(truthy_value[index] , " == false");
    }
 }
```

Output :

```
{}  == true
[]  == true
{ name: 1 }  == true
[ 1 ]  == true
[ '1' ]  == true
100  == true
0  == true
false  == true
Infinity  == true
-Infinity  == true
true  == true
[String: '']  == true
-100  == true
1n  == true
1.1  == true
-1.1  == true
true  == true
   == true
```

As represented in the above code snippet, empty and non-empty arrays and objects, along with negative and positive numbers and floats (decimals) datatype, bigInt, strings, and keywords like 'true' and 'Infinity' were tested inside the if statement. These all values return 'true' when checked with the if-else statement.

According to the definition of truthy values, the 'true' keyword is not checked inside the if statement but still code for the if statement gets executed for all values in the truthy_value array, which means that all the given array elements are non-falsy values. You will observe this application of truthy values in almost every code since it is easier than using the 'true' keyword. This increases the neatness of code and saves many lines of code as the 'true' keyword is not required to be returned from the functions. 

Along with the datatypes, the comparison operator also can return truthy values, as shown in the code snippet below.

```
let first_number = 10, second_number  = 10;
( first_number == second_number)? console.log("first_number is equal to second_number"): console.log("first_number is not equal to second_number");

first_number = 10, second_number = 20;
( first_number < second_number)? console.log("first_number is less than second_number"): console.log("first_number is greater than second_number");

first_number = 20, second_number = 10;
(first_number > second_number)? console.log("first_number is greater than second_number"): console.log("first_number is less than second_number");

console.log("");

// Logical OR operator
first_variable = 'true', second_variable = 'truthy_value';
result = (first_variable || second_variable);
if(result){
    console.log(result);
}

// Logical AND operator
first_variable = 'true', second_variable  = 'truthy_value';
result = (first_variable && second_variable);
if(result){
    console.log(result);
}
```

Output :

```
first_number is equal to second_number
first_number is less than second_number
first_number is greater than second_number

true
truthy_value
```
From the above code, we can see that truthy values are also obtained when relational and logical operators are used and thus these comparisons are used to substitute the 'true' keyword. Another thing, we can observe is that if a logical AND operator evaluates to true, the value of second operand is returned instead of a true keyword.

<br>

## Falsy Values 

A Falsy value, also known as a Falsey value, is defined as the values returned by a code or statement, that evaluates to 'false'. Just like every non-falsey value gives a 'true' boolean, similarly every non-truthy values give a 'false' boolean.

Similar to truthy values, javascript use type coercion to convert the output of a line of code into a false boolean value.

The following code block shows a falsey_value array that consists of all values in Javascript that are interpreted as false boolean type.

```
falsey_value = [false, -0, 0,-0.0,0.0, "", '', NaN, 0n, null, undefined ]
 
for ( let index = 0; index < falsey_value.length; index++){
    if (falsey_value[index]){
    console.log(falsey_value[index] , " == true");
    }
    else{
    console.log(falsey_value[index] , " == false");
    }
 }
```

Output :
```
false  == false
-0  == false
0  == false
-0  == false
0  == false
  == false
  == false
NaN  == false
0n  == false
null  == false
undefined  == false
```

The above code block have all type of falsey values in an array and is checked by an if-else statement for its output as shown above. The if statement for any array value did not evaluated to 'true' since they represents 'false' boolean value. NaN (Not-A-Number), null, undefined, and false is the keyword are returned as falsey values and all numbers, floats, and BigInt representing 0, empty string also returns falsey values.

The falsey values are also returned for logical and comparison operators. The example is shown below,

```
let first_number = 10, second_number  = 20;
(first_number == second_number)? console.log("first_number is equal to second_number"): console.log("first_number is not equal to second_number");

(first_number > second_number)? console.log("first_number is greater than second_number"): console.log("first_number is less than second_number");

first_number = 20, second_number = 10;
(first_number < second_number)? console.log("first_number is less than second_number"): console.log("first_number is greater than second_number");

console.log("");

// Logical OR operator
first_variable = 0 , second_variable = "";
result = (first_variable || second_variable);
if(result){
    console.log(result);
}else{
    console.log("Logical OR operator return a falsey value.");
}

// Logical AND operator
first_variable = 'true', second_variable = false;
result = (first_variable && second_variable);

if(result){
    console.log(result);
}else{
    console.log("Logical AND operator return a falsey value.");
}
```

Output :

```
first_number is not equal to second_number
first_number is less than second_number
first_number is greater than second_number
Logical OR operator returns a falsey value.
Logical AND operator returns a falsey value.
```
We can see that falsey values are also obtained when relational and logical operators are used and this help to substitute 'false' keyword.

In a case, where we need to check whether a value is truthy or falsey we can use the Boolean function in Javascript, as shown below.

```
console.log(Boolean(1));
console.log(Boolean({}));

console.log(Boolean(0));
console.log(Boolean(NaN));

```

Output :

```
true
true
false
false
```

