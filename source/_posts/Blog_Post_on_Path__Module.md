---
title: Path Module Methods
date: 2023-03-15
tags:
---

'Path' is a very useful module while dealing with the paths of the different files and directories in a project. In this blog, we will understand what is 'path' module in Node.js and its two important methods 'path.join()' and 'path.resolve()'. Before going to methods, we will first understand some basic concepts of the 'path' module.

## What is Path Module?

The 'Path' module is a Node.js built-in module that is very useful while working with files and directories. It provides the methods and properties to work with files and directories paths.

This module can be imported by using the built-in require function;

```js
const path = require('path');
```
In this syntax, the path inside the 'require' function indicates the name of the built-in module to be imported inside the JavaScript file. After running this code, we can now work with files and directories using 'path' module methods. 

To start with using the 'path' module, we need a path to use the modules method. For this, we can run the '__filename' and '__dirname' variables. '__filename' gives us the absolute path string of the file we are working with, and the '__dirname' variable gives the path string of the folder in which the current file is located.

This can be seen in the code example below;
```js
const path = require('path');

filePath = __filename;
directoryPath = __dirname;
console.log("File Path :\n" + __filename);
console.log("Directory Path :\n" + __dirname);
```
Output :

```
File Path :
/home/acer/Desktop/Path_Modules.js
Directory Path :
/home/acer/Desktop
```
Now, that we stored the paths of our current file and the folder inside the respective variables, we can start with the first method of 'path' modules which is basename. 

The 'basename' method of the 'path' module is used to output the last portion of a path given as an argument. This is mostly used to extract the file name from an absolute path or the last folder name of a path.

In this code, we will use the 'basename' method on our previously stored file path and directory path.

```js
const path = require('path');

filePath = __filename;
directoryPath = __dirname;

console.log(path.basename(filePath));
console.log(path.basename(directoryPath));
```
Output :
```
Path_Modules.js
Desktop
```
As displayed from the output, the file path's last portion is returned by the basename method is 'Path_modules.js', which consists of the name of the file and its extension as well. In case of 'directoryPath', the basename is the folder name at the last portion of the directory string, in which our file is located.

The next method for 'path' modules is 'dirname', which returns the directory path of a complete path to a file or a folder.

```js
const path = require('path');

filePath = __filename;
directoryPath = __dirname;

console.log(path.dirname(filePath));
console.log(path.dirname(directoryPath));
```
Output :
```
/home/acer/Desktop
/home/acer
```

Here, the 'dirname' name returns the entire path for our file, 'Path_Modules.js' and for the directory path as input to the 'dirname' method, the path to the last folder 'Desktop' is returned.

## Path Modules Methods : 

### ```path.join()``` 

The path.join() is used for joining all the segments of a path into a single path. These segments are joined using a platform-specific separator, which is a forward slash('/') in the case of a Unix-based system.  

To use '.join()' method, we input the segments of a path, in the required order inside the parenthesis

Let us append a newly created file "File_1.json" to the 'directoryPath' variable,

```js
const path = require('path');

filePath = __filename;
directoryPath = __dirname;

console.log(path.join(directoryPath, "File_1.json"));
```
Output :
```
/home/acer/Desktop/File_1.json
```
Here, we got the entire directory attached to the new file "File_1.json", without the need of adding a separator. Along with joining segments of the path, '.join()' method also corrects the given path components as shown below,

```js
const path = require('path');

filePath = __filename;
directoryPath = __dirname;

console.log(path.join(directoryPath, "//File_1.json"));
console.log("\nCreate path for index.html")
console.log(path.join(directoryPath, "//index.html"));
console.log("\nCreate path for File_2.py")
console.log(path.join("./",directoryPath, "../File_2.py"));
```
Output :
```
/home/acer/Desktop/File_1.json

Create path for index.html
/home/acer/Desktop/index.html

Create path for File_2.py
/home/acer/File_2.py
```
In the input for index.html, we provided a double slash in the file argument, but the join method rectified it and only added one slash. For 'File_2.py', we added double periods "." before the forward slash, which got interpreted as travel to the previous directory, and therefore the directory for 'File_2.py" is in the previous folder, as compared to the actual directory path.



### ```path.resolve``` 

'path.resolve' method converts or resolves a sequence of path arguments, to an absolute path. It returns the path which is absolute by joining the given path components with the current working directory. 

Let's understand the resolve method by the code below,

```js
const path = require('path');

filePath = __filename;
directoryPath = __dirname;

console.log(path.resolve(directoryPath, "directory_1","File_1.json"));
console.log("\nCreate path for index.html")
console.log(path.resolve(directoryPath,"/directory_1", "index.html"));
console.log("\nCreate path for File_2.py")
console.log(path.resolve(directoryPath,"/directory_1", "/File_2.py"));
```
Output :
```
/home/acer/Desktop/directory_1/File_1.json

Create path for index.html
/directory_1/index.html

Create path for File_2.py
/File_2.py
```

In the output above, when we append "File_1.json" to the current directory, the absolute path started from the root of the directory Path given. In the case of 'index.html', we added a forward slash in front of the "directory_1" folder, and therefore the resolve method started the absolute path from the place of the forward slash, before directory_1. Finally, in the case of File_2.py, our absolute path started from the file, because we added a forward slash at the front of the file name.





