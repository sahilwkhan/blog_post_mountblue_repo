---
title: Promise Methods
date: 2023-03-14
tags:
---

# What is a Promise in JavaScript?

A Promise in JavaScript is a returned object, that is used to handle the output of asynchronous operations. In previous versions of JavaScript, the error-first callback functions were used as a parameter to asynchronous functions. In the case of using multiple asynchronous functions one after the other, we need to pass a callback function every time to a new asynchronous function, which made it very difficult to trace bugs.

An example of three asynchronous functions using the callback functions approach is given below :

```js
const fs = require("fs");

// Read lowerCaseFile.txt file from current directory
fs.readFile("./lowerCaseFile.txt", 'utf8', (error1, lowerCaseData) => {
    if (error1) {
        return console.log(error1);
    }
    else {
        let upperCaseData = lowerCaseData.toUpperCase();

        // Create and write the data in upperCaseFile.txt file
        fs.writeFile("./upperCaseFile.txt", upperCaseData, function (error2) {
            if (error2) {
                return console.log("Error writing file", error2);
            }
            else {
                console.log(`New file with UpperCase text is created successfully.`);

                // Append the name of upperCaseFile.txt inside filenames.txt
                fs.appendFile("./filenames.txt", "upperCase.txt", (error3) => {
                    if (error3) {
                        return console.log(error3);
                    }
                    else {
                        console.log(`upperCase.txt added to filenames.txt`);
                    }
                });
            
            }
        });
    }
});
```

The code above shows that just by using three fs asynchronous functions, it became difficult to read code as it is expanding in the horizontal direction. In the case of adding more asynchronous functions to this code, it becomes even more difficult to read and maintain code, and increases the difficulty to trace bugs. This stacking of asynchronous functions by using callback functions creates a callback pyramid of doom, also known as callback hell.

ECMAScript 2015, also known as ES6 introduced the concept of a promise object. The promises object also uses callback functions to deal with asynchronous functions, but in promises, callback functions are attached to the promise objects that are returned from asynchronous operations. This ability to add callback functions to the returned promises allows us to prevent the callback pyramid of doom.

The code below shows the previous example of an asynchronous function regarding file handling, by using promise,

```js
const fs = require("fs");

new Promise ( (resolve, reject)=>{
    // Read lowerCaseFile.txt file from current directory
    fs.readFile("./lowerCaseFile.txt", 'utf8', (error1, lowerCaseData) => {
        if (error1) {
            reject(error1);
        }
        else {
            console.log("lowerCaseFile read successfully.");
            let upperCaseData = lowerCaseData.toUpperCase();
            resolve(upperCaseData);
        }  
    });

})
.then( (upperCaseData)=>{
    return new Promise( (resolve,reject) =>{
        // Create and write the data in upperCaseFile.txt file
        fs.writeFile("./upperCaseFile.txt", upperCaseData, function (error2) {
            if (error2) {   
                reject(error2);
            }
            else {
                console.log("upperCaseFile.txt created successfully.");
                resolve("upperCaseFile.txt"); 
            }
        });    
    })
})
.then( (upperCaseFileName) =>{
    return new  Promise( (resolve,reject) =>{
        // Append the name of upperCaseFile.txt inside filenames.txt
        fs.appendFile("./filenames.txt" , upperCaseFileName, function (error3) {
            if (error3) {
                reject(error3);
            }
            else {
                console.log(`Added ${upperCaseFileName} to the filenames.txt`);
                resolve();
            }
    });
    })
})
.catch( (error)=>{
    console.log(error);
});
```
In the above code, the promises that were received from asynchronous functions were added with the callback functions using '.then()' method. The promise returned shows the result of the asynchronous operation performed. There are three states of a promise object.
1. Pending state, which is the initial state of any promise.
2. Fulfiled state, which represents that the asynchronous function was completed.
3. Rejected state, which shows that asynchronous function failed to complete.
   
In this example, at first, the promise was returned for readFile functions. If data was successfully read from the 'lowerCaseFile.txt', the promise is resolved and output is then given to the first '.then()' method. This method is used to handle the promises that are resolved. The output from the readFile functions is given to the writeFile function, and runs the 'writefile' function. After it is completed, the second '.then()' method is passed with the filename as output from the writeFile function.
Here, the name of the new file created is added to the filenames.txt folder. In this complete process, if any error occurs, it rejects the promise and any error is transferred to the '.catch()' method, just like try-catch in synchronous programming.


# Promises Method :

## Promise.all() 

'Promises.all()' is a static method that operates on a sequence of promises. It is used to execute the promises in the given list concurrently. After iterating on all the promises, the 'Promises.all()' method returns a single promise with the fulfilled state if all the promises of the list or sequence return a fulfilled state or resolve functions. If any of the promises in the list returns rejection, the 'Promises.all()' will return rejection without checking further promises.

Example of Promise.all() :

```js
const promise1 = new Promise((resolve, reject) => {
  setTimeout(resolve, 1000, 'Promise1 ');
});

const promise2 = new Promise((resolve, reject) => {
  setTimeout(resolve, 1000, 'Promise2');
});

const promise3 = new Promise((resolve, reject) => {
  setTimeout(resolve, 1000, 'Promise3');
});

Promise.all([promise1, promise2, promise3]).then((values) => {
  console.log(values);
});
```

Output :
```
[ 'Promise1 ', 'Promise2', 'Promise3' ]
```

In this code, promise1 is resolved with the string 'Promise1', promise2 is resolved with the string 'Promise2' and promise3 is resolved with the string 'Promise3'. An array of all these promises is given as input to the 'Promise.all()'. Since all promises return fulfilled state, the '.then()' method is passed with a single promise with an array of all promises values, logged by the console statement. This array of values will be logged to the console, after 1 second because all promises run concurrently.

In case anyone promise is not resolved then the promise returned by the 'Promise.all()' will throw an error.

This case is shown in the code below,

```js
const promise1 = new Promise((resolve, reject) => {
  setTimeout(reject, 1000, 'Promise1 ');
});

const promise2 = new Promise((resolve, reject) => {
  setTimeout(resolve, 1000, 'Promise2');
});

const promise3 = new Promise((resolve, reject) => {
  setTimeout(resolve, 1000, 'Promise3');
});

Promise.all([promise1, promise2, promise3]).then((values) => {
  console.log(values);
})
```
Output :
```
(node:1891) UnhandledPromiseRejectionWarning: Promise1 
(node:1891) UnhandledPromiseRejectionWarning: Unhandled promise rejection. This error originated either by throwing inside of an async function without a catch block, or by rejecting a promise which was not handled with .catch(). To terminate the node process on unhandled promise rejection, use the CLI flag `--unhandled-rejections=strict` (see https://nodejs.org/api/cli.html#cli_unhandled_rejections_mode). (rejection id: 1)
(node:1891) [DEP0018] DeprecationWarning: Unhandled promise rejections are deprecated. In the future, promise rejections that are not handled will terminate the Node.js process with a non-zero exit code.
```

As it can be seen, when the reject is returned by promise1, the final promise also returns an error without evaluating promise2 and promise3. 

## Promise.allSettled() 

Promise.allSettled is another concurrency method of promise, where all promises are settled irrespective of the state they return to. This method is preferred when we have to run multiple asynchronous operations that are independent of other each other. 

Example of Promise.allSettled() :

```js
const promise1 = Promise.resolve(1000)

const promise2 = new Promise((resolve, reject) => {
  setTimeout(resolve, 1000,"Promise2 is fulfilled");
});

const promise3 = new Promise((resolve, reject) => {
  setTimeout(reject, 1000, 'Promise3 is rejected');
});

Promise.allSettled([promise1, promise2, promise3]).then((values) => {
  console.log(values);
})
```
Output :
```
[
  { status: 'fulfilled', value: 1000 },
  { status: 'fulfilled', value: 'Promise2 is fulfilled' },
  { status: 'rejected', reason: 'Promise3 is rejected' }
]
```

'Promise.allSettled()' method takes a sequence of promises and returns a single promise. This promise gives an object that shows all states of promises, whether a promise is rejected or fulfilled, and their value if the promise is fulfilled, and if any promise of sequence gives rejection, the reason for the rejected promise is displayed.  

## Promise.any()

Promise.any() is a static method in JavaScript that takes an array of promises and returns a new promise that resolves when any of the promises have been resolved. The returned promise is rejected when all of the input promises are rejected. 

Example of Promise.any() :

```js
const promiseArray = [Promise.reject(new Error("Rejection Promise")), Promise.resolve("First Fulfilled Promise"),Promise.resolve("Second Fulfilled Promise"), Promise.reject(new Error( "Failed Promise"))]

Promise.any(promiseArray)
.then((values)=>{
    console.log(values);
})
```

Output :
```
"First Fulfilled Promise"
```

When more than one promise in the array returns fulfilled state, the promise returned by the Promise.any() is the value of first fulfilled state promise. 

Example of Promise.any() when all promises in the array rejects :

```js
const promiseArray = [Promise.reject(new Error("Rejection Promise"),"asd"), Promise.reject(new Error( "Failed Promise")),Promise.reject(new Error( "Second Failed Promise"))]

Promise.any(promiseArray)
.then((values)=>{
    console.log(values);
})
.catch((error)=>{
  console.log(error);
})
```

Output :
```
AggregateError: All promises were rejected
```
When all of the promises in the input array are rejected, the new promise is rejected with an AggregateError. This error displays the array of rejections for all the rejected promises.



## Promise.race()

In 'Promise.race' method, an array of promises are given as input and the method returns a single promise. This single promise result is based on the promise of the array which resolves fastest, whether in reject condition or fulfilled state.

Example of Promise.race() method when the first promise is resolved :
```js
const resolvedPromise1 = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve('Promise 1 resolved');
  }, 1000);
});

const resolvedPromise2 = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve('Promise 2 resolved');
  }, 2000);
});

const rejectPromise = new Promise((resolve, reject) => {
  setTimeout(() => {
    reject('Promise rejected');
  }, 2000);
});

Promise.race([resolvedPromise1, resolvedPromise2,rejectPromise]).then((result) => {
  console.log(result); // logs "Promise 1 resolved"
})
.catch((error)=>{
  console.log(error);
})
```
Output :
```
"Promise 1 resolved"
```
From output, we can see that the promise1 value is returned as the single promise output, because it completed first among the other promises.

Example of Promise.race() method when the first promise is rejected :
```js
const resolvedPromise1 = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve('Promise 1 resolved');
  }, 1000);
});

const resolvedPromise2 = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve('Promise 2 resolved');
  }, 2000);
});

const rejectPromise = new Promise((resolve, reject) => {
  setTimeout(() => {
    reject('Promise rejected');
  }, 500);
});

Promise.race([resolvedPromise1, resolvedPromise2,rejectPromise]).then((result) => {
  console.log(result); // logs "Promise 1 resolved"
})
.catch((error)=>{
  console.log(error);
})
```
Output :
```
"Promise rejected"
```
In this example, the 'rejectPromise' has completed first and returned rejected state, therefore the 'Promise.race()' returned the value of 'rejectPromise' inside '.catch()' method.
